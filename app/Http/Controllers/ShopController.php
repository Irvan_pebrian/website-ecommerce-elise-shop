<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Produk;
use App\Keranjang;
use App\HistoryPenjualan;
use Illuminate\Support\Facades\Cookie;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use Auth;

class ShopController extends Controller
{

  public function index(){

    if (!Cookie::get('unique')) {
      $data = str_random(15);
      Cookie::queue('unique', $data);
    }

    $cart = Keranjang::where('current_pc' , Cookie::get('unique'))->get();
    $total_harga = Keranjang::where('current_pc' , Cookie::get('unique'))->sum('harga_barang');

    return view('User.index' , ['list_cart' => $cart])->with('tCart',count($cart))->with('total_harga',$total_harga);
  }

  public function getView($key){

    if ($key === 't-shirts') {

      $data = Produk::all();
      $cart = Keranjang::where('current_pc' , Cookie::get('unique'))->get();
      return view('User.t-shirts' , ['produk' => $data])->with('total',count($data))->with('tCart',count($cart));

    }elseif ($key === 'jackets') {

      return view('User.jackets');

    }

  }

  public function toCart($id){

    $getProduk = Produk::find($id);

    $data = new Keranjang;
    $data->current_pc = Cookie::get('unique');
    $data->id_barang = $id;
    $data->nama_barang = $getProduk['Nama'];
    $data->harga_barang = $getProduk['Harga'];
    $data->foto_barang = $getProduk['foto'];
    $data->save();

    $getAll = Keranjang::where('current_pc' , Cookie::get('unique'))->get();

    return redirect('/shop/t-shirts');

  }

  public function clearCart(){
    $data = Keranjang::where('current_pc',Cookie::get('unique'));
    if ($data->delete()) {
      return redirect('/');
    }else {
      return redirect('/');
    }
  }

  public function checkOut(){

    try {
      $client = new Client();
      $response = $client->request('GET', 'https://api.rajaongkir.com/starter/province',[
          'headers' => [
              'key' => '99a5fa33f9b2250d1c6c1587a050d4fc',
              'Accept' => 'application/json',
          ]
      ]);
      if ($response->getStatusCode() == 200) { // 200 OK
          $response_data = $response->getBody()->getContents();
          $data = json_decode($response_data,true);
          $provinsi = $data['rajaongkir']['results'];
      }
    } catch (ClientException $e) {
      echo "<script>alert('Terjadi Kesalahan')</script>";
    }

    try {
      $client = new Client();
      $response_kota = $client->request('GET', 'https://api.rajaongkir.com/starter/city',[
          'headers' => [
              'key' => '99a5fa33f9b2250d1c6c1587a050d4fc',
              'Accept' => 'application/json',
          ]
      ]);
      if ($response_kota->getStatusCode() == 200) { // 200 OK
          $response_data_kota = $response_kota->getBody()->getContents();
          $data_kota = json_decode($response_data_kota,true);
      }
    } catch (ClientException $e) {
      echo "<script>alert('Terjadi Kesalahan')</script>";
    }

    if(Auth::user()){
      $cart = Keranjang::where('current_pc' , Cookie::get('unique'))->get();
      $total_harga = Keranjang::where('current_pc' , Cookie::get('unique'))->sum('harga_barang');
      return view('User.checkout' , ['list_cart' => $cart])
      ->with('tCart',count($cart))
      ->with('provinsi' , $provinsi)
      ->with('kota' , $data_kota['rajaongkir']['results'])
      ->with('total_harga',$total_harga);
    }else{
      return redirect('/login');
    }

  }

  public function placeOrder(Request $request){

    $data = $request->all();
    $i = 1;
    do {
        $orderan = new HistoryPenjualan;
        $orderan->first_name = $data['first_name'];
        $orderan->last_name = $data['last_name'];
        $orderan->post_code = $data['first_name'];
        $orderan->address = $data['address'];
        $orderan->post_code = $data['postcode'];
        $orderan->kota = $data['kota'];
        $orderan->provinsi = $data['provinsi'];
        $orderan->phone_no = $data['phone'];
        $orderan->email = $data['email'];
        $orderan->total_barang = $data['total_barang'];
        $orderan->nama_barang = $data['nama_barang_'.$i];
        $orderan->current_pc = Cookie::get('unique');
        $orderan->save();
        $i++;
    } while ($i <= (int)$data['total_barang']);

    $clearCart = Keranjang::where('current_pc',Cookie::get('unique'));
    $clearCart->delete();

    $cart = Keranjang::where('current_pc' , Cookie::get('unique'))->get();
    $total_harga = Keranjang::where('current_pc' , Cookie::get('unique'))->sum('harga_barang');
    return redirect('/')
    ->with('list_cart',$cart)
    ->with('tCart',count($cart))
    ->with('total_harga',$total_harga);

  }

}
