<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Produk;
use App\HistoryPenjualan;
use Illuminate\Support\Facades\DB;
use Auth;

class AdminController extends Controller
{
    
    public function index(){
        if (Auth::user()->name === 'admin') {
            return view('Admin.layout.admin');   
        }else if(Auth::user() === null){
            return redirect('/login');
        }else{
            return redirect('/');
        }
    }

    public function kelolaBarang(){
        $data = Produk::all();
        return view('Admin.barang.barang' , ['barang' => $data]);
    }

    public function tambahBarang(){
        return view('Admin.barang.tambah');
    }

    public function storeBarang(Request $request){
        
        $data = new Produk;
        $data->ID = rand(50,100000);
        $data->Nama = $request->nama;
        $data->Harga = $request->harga;
        $data->Kategori = $request->kategori;
        $data->Stok = $request->stok;

        if ($data->save()) {
            return redirect('/kelola-barang');
        }else{
            return redirect('/kelola-barang');
        }

    }

    public function hapusBarang($id_hapus){
        DB::table('produk')->where('ID',$id_hapus)->delete();
        return redirect('/kelola-barang');
    }

    public function historyPembelian(){
        $data = HistoryPenjualan::all();
        return view('Admin.history.history-pembelian' , ['history' => $data]);
    }

    public function getBarang($id){
        $data = Produk::where('ID',$id)->first();
        return view('Admin.barang.edit' , ['data' => $data]);
    }

    public function editBarang(Request $request){

        DB::table('produk')->where('ID',$request->id)->update([
			'Nama' => $request->nama,
			'Harga' => $request->harga,
			'Kategori' => $request->kategori,
			'Stok' => $request->stok
		]);

        return redirect('/kelola-barang');
        
    }

}
