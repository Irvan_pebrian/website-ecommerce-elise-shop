<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HistoryPenjualan extends Model
{
  protected $table = 'history_pembelian';
  protected $primary_key = 'id_history';
  public $timestamps = false;
}
