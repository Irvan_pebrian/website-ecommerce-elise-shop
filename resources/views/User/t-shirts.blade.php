@extends('User.layout.shop')

@section('title' , 'T-Shirts Collection')
@section('Judul-Produk' , 'T-Shirts')

@section('jumlah-produk')
<?= $total ?>
@endsection

@section('total-cart')
  <?php if ($tCart === null): ?>
    0
  <?php endif; ?>
  <?= $tCart ?>
@endsection


@section('konten-produk')

<!-- Pengulangan Barang -->

<?php foreach ($produk as $a): ?>
  <div class="col-12 col-sm-6 col-lg-4">
      <div class="single-product-wrapper">
          <!-- Product Image -->
          <div class="product-img">
              <img src="{{ url('/uploads/' . $a->foto) }}" alt="">
          </div>

          <!-- Product Description -->
          <div class="product-description">
              <span>Elise Shop</span>
              <h6>{{ $a->Nama }}</h6>
              <p class="product-price">Rp.{{ $a->Harga }}</p>

              <!-- Hover Content -->
              <div class="hover-content">
                  <!-- Add to Cart -->
                  <div style="color : #fff" class="add-to-cart-btn">
                    <a href="{{ url('/shop/keranjang/' . $a->ID) }}" class="btn essence-btn">Add to Cart</a>
                  </div>
              </div>
          </div>
      </div>
  </div>
<?php endforeach; ?>

<script type="text/javascript">

  $('#addToCart').click(function(e){
    e.preventDefault()
    alert(e.target.value)
  })

</script>

<!-- Akhir Pengulangan Barang -->

@endsection()
