@extends('User.layout.master')
@section('judul' , 'Elise Shop')

@section('total-cart')
<?php if ($tCart === null): ?>
  0
<?php endif; ?>
<?= $tCart ?>
@endsection

@section('total-harga')
<?php if ($total_harga === null): ?>
  0
<?php endif; ?>
<?= $total_harga ?>
@endsection

@section('list-cart')

<?php foreach ($list_cart as $b): ?>
  <div class="single-cart-item">
      <a href="#" class="product-image">
          <img src="{{ url('/uploads/' . $b->foto_barang) }}" class="cart-thumb" alt="">
          <!-- Cart Item Desc -->
          <div class="cart-item-desc">
            <span class="product-remove"><i class="fa fa-close" aria-hidden="true"></i></span>
              <span class="badge">Elise Shop</span>
              <h6>{{ $b->nama_barang }}</h6>
              <p class="size">Size: Random (S/M/L/XL)</p>
              <p class="price">RP.{{ $b->harga_barang }}</p>
          </div>
      </a>
  </div>
<?php endforeach; ?>

@endsection
