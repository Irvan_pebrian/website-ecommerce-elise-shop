<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="description" content="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- The above 4 meta tags *must* come first in the head; any other head content must come *after* these tags -->

    <!-- Title  -->
    <title>Checkout Barang</title>

    <!-- Favicon  -->
    <link rel="icon" href="/essence/img/core-img/favicon.ico">

    <!-- Core Style CSS -->
    <link rel="stylesheet" href="/essence/css/core-style.css">
    <link rel="stylesheet" href="/essence/style.css">

</head>

<body>
    <!-- ##### Header Area Start ##### -->
    <header class="header_area">
        <div class="classy-nav-container breakpoint-off d-flex align-items-center justify-content-between">
            <!-- Classy Menu -->
            <nav class="classy-navbar" id="essenceNav">
                <!-- Logo -->
                <a class="nav-brand" href="/"><img src="/essence/img/core-img/logo.png" alt=""></a>
                <!-- Navbar Toggler -->
                <div class="classy-navbar-toggler">
                    <span class="navbarToggler"><span></span><span></span><span></span></span>
                </div>
                <!-- Menu -->
                <div class="classy-menu">
                    <!-- close btn -->
                    <div class="classycloseIcon">
                        <div class="cross-wrap"><span class="top"></span><span class="bottom"></span></div>
                    </div>
                    <!-- Nav Start -->
                    <div class="classynav">
                        <ul>
                            <li><a href="#">Shop</a>
                                <div class="megamenu">
                                    <ul class="single-mega cn-col-4">
                                        <li class="title">Men's Collection</li>
                                        <li><a href="/shop/t-shirts">T-Shirts</a></li>
                                        <li><a href="/shop/jacket">Jackets</a></li>
                                    </ul>
                                    <div class="single-mega cn-col-4">
                                        <img src="/essence/img/bg-img/bg-6.jpg" alt="">
                                    </div>
                                </div>
                            </li>
                            <li><a href="#">Blog</a></li>
                            <li><a href="#">Contact</a></li>
                        </ul>
                    </div>
                    <!-- Nav End -->
                </div>
            </nav>

            <!-- Header Meta Data -->
            <div class="header-meta d-flex clearfix justify-content-end">
                <!-- Search Area -->
                <div class="search-area">
                    <form action="#" method="post">
                        <input type="search" name="search" id="headerSearch" placeholder="Type for search">
                        <button type="submit"><i class="fa fa-search" aria-hidden="true"></i></button>
                    </form>
                </div>
                <!-- Favourite Area -->
                <div class="favourite-area">
                    <a href="#"><img src="/essence/img/core-img/heart.svg" alt=""></a>
                </div>
                <!-- User Login Info -->
                <div class="user-login-info">
                    <a href="#"><img src="/essence/img/core-img/user.svg" alt=""></a>
                </div>
                <!-- Cart Area -->
                <div class="cart-area">
                    <a href="#" id="essenceCartBtn"><img src="/essence/img/core-img/bag.svg" alt=""> <span>@yield('total-cart')</span></a>
                </div>
            </div>

        </div>
    </header>

    <div class="checkout_area section-padding-80">
        <div class="container">
            <div class="row">

                <div class="col-12 col-md-6">
                    <div class="checkout_details_area mt-50 clearfix">

                        <div class="cart-page-heading mb-30">
                            <h5>Billing Address</h5>
                        </div>

                        <form action="{{ url('/bayar') }}" method="post">
                          @csrf
                            <div class="row">
                                <div class="col-md-6 mb-3">
                                    <label for="first_name">First Name <span>*</span></label>
                                    <input type="text" class="form-control" id="first_name" name="first_name" value="" required>
                                </div>
                                <div class="col-md-6 mb-3">
                                    <label for="last_name">Last Name <span>*</span></label>
                                    <input type="text" class="form-control" id="last_name" name="last_name" value="" required>
                                </div>
                                <div class="col-12 mb-3">
                                    <label for="street_address">Address <span>*</span></label>
                                    <input type="text" class="form-control mb-3" name="address" id="street_address" value="">
                                </div>
                                <div class="col-12 mb-3">
                                    <label for="postcode">Postcode <span>*</span></label>
                                    <input type="text" class="form-control" name="postcode" id="postcode" value="">
                                </div>
                                <div class="col-12 mb-3">
                                    <label for="state">Kota <span>*</span></label><br>
                                    <select name="kota" style="width:500px">
                                      <?php foreach ($kota as $r): ?>
                                        <option value=<?= $r['city_id'] ?>> <?= $r['city_name'] ?> </option>
                                      <?php endforeach; ?>
                                    </select>
                                </div>
                                <div class="col-12 mb-3">
                                    <label for="state">Province <span>*</span></label><br>
                                    <select name="provinsi" style="width:500px">
                                      <?php foreach ($provinsi as $z): ?>
                                        <option value=<?= $z['province_id'] ?>> <?= $z['province'] ?> </option>
                                      <?php endforeach; ?>
                                    </select>
                                </div>
                                <div class="col-12 mb-3">
                                    <label for="phone_number">Phone No <span>*</span></label>
                                    <input type="text" name="phone" class="form-control" id="phone_number" value="">
                                </div>
                                <div class="col-12 mb-4">
                                    <label for="email_address">Email Address <span>*</span></label>
                                    <input type="email" name="email" class="form-control" id="email_address" value="">
                                </div>
                            </div>
                          </div>
                        </div>

                <div class="col-12 col-md-6 col-lg-5 ml-lg-auto">
                    <div class="order-details-confirmation">

                        <div class="cart-page-heading">
                            <h5>Your Order</h5>
                            <p>The Details</p>
                        </div>

                        <ul class="order-details-form mb-4">
                            <li><span>Product</span> <span><?= $tCart ?> Barang</span></li>
                            <input style="display:none" type="text" name="total_barang" class="form-control" value=<?= $tCart ?>>
                            <?php $i = 1; ?>
                            <?php foreach ($list_cart as $b): ?>
                              <li><span>-{{ $b->nama_barang }}</span> <span>RP.{{ $b->harga_barang }}</span></li>
                              <input style="display:none" type="text" name="nama_barang_<?= $i++ ?>" class="form-control" value=<?= $b['nama_barang'] ?>>
                            <?php endforeach; ?>
                            <li><span>Subtotal</span> <span>RP.<?= $total_harga ?></span></li>
                            <li><span>Shipping</span> <span>Free</span></li>
                            <li><span>Total</span> <span>RP.<?= $total_harga ?></span></li>
                        </ul>
                        <button type="submit" class="btn essence-btn">Place Order</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
    </footer>
    <!-- ##### Footer Area End ##### -->

    <!-- jQuery (Necessary for All JavaScript Plugins) -->
    <script src="/essence/js/jquery/jquery-2.2.4.min.js"></script>
    <!-- Popper js -->
    <script src="/essence/js/popper.min.js"></script>
    <!-- Bootstrap js -->
    <script src="/essence/js/bootstrap.min.js"></script>
    <!-- Plugins js -->
    <script src="/essence/js/plugins.js"></script>
    <!-- Classy Nav js -->
    <script src="/essence/js/classy-nav.min.js"></script>
    <!-- Active js -->
    <script src="/essence/js/active.js"></script>

</body>

</html>
