@extends('Admin.layout.admin')
@section('judul', 'Kelola Barang')

@section('content')
<div class="card">
  <div class="card-header">
    Tabel Barang
    <a href="{{ url('/barang/tambah') }}" style="float : right" class="btn btn-primary">Tambah Barang</a>
  </div>
  <div class="card-body">
  <table class="table table-striped">
  <thead>
    <tr>
      <th scope="col">ID</th>
      <th scope="col">Nama</th>
      <th scope="col">Harga</th>
      <th scope="col">Kategori</th>
      <th scope="col">Foto</th>
      <th scope="col">Stok</th>
      <th scope="col">Handle</th>
    </tr>
  </thead>
  <tbody>
    <?php foreach ($barang as $a): ?>
        <tr>
            <td>{{ $a->ID }}</td>
            <td>{{ $a->Nama }}</td>
            <td>{{ $a->Harga }}</td>
            <td>{{ $a->Kategori }}</td>
            <td>
                <img style="width : 70px" src="{{ url('/uploads/' . $a->foto) }}" alt="">
            </td>
            <td>{{ $a->Stok }}</td>
            <td>
                <a href="{{ url('/barang/edit/' . $a->ID) }}" class="btn btn-warning">Edit Data</a>
                <form action="{{ url('/barang/hapus/' . $a->ID) }}" method="post">
                  @csrf
                  <button type="submit" style="color : #fff" class="btn btn-danger">Hapus</a>
                </form>
            </td>
        </tr>
    <?php endforeach; ?>
  </tbody>
</table>
  </div>
</div>
@endsection