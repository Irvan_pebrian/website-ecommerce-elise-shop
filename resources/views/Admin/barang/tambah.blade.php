@extends('Admin.layout.admin')
@section('judul', 'Kelola Barang')

@section('content')
<div class="card">
  <div class="card-header">
    Tambah Barang
  </div>
  <div class="card-body">
    
    <form action="/store-barang" method="post">
        @csrf
        <div class="form-group">
            <label>Nama</label>
            <input type="text" name="nama" class="form-control" placeholder="Masukan Nama Barang">
        </div>
        <div class="form-group">
            <label>Harga</label>
            <input type="text" name="harga" class="form-control" placeholder="Masukan Harga Barang">
        </div>
        <div class="form-group">
            <label>Kategori</label>
            <input type="text" name="kategori" class="form-control" placeholder="Masukan Kategori Barang">
        </div>
        <div class="form-group">
            <label>Foto</label>
            <input type="file" name="file" class="form-control">
        </div>
        <div class="form-group">
            <label>Stok</label>
            <input type="text" name="stok" class="form-control" placeholder="Masukan Stok Barang">
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>

  </div>
</div>
@endsection