@extends('Admin.layout.admin')
@section('judul', 'Kelola Barang')

@section('content')
<div class="card">
  <div class="card-header">
    Tabel History Pembelian
  </div>
  <div class="card-body">
  <table class="table table-striped">
  <thead>
    <tr>
      <th scope="col">Id History</th>
      <th scope="col">First Name</th>
      <th scope="col">Last Name</th>
      <th scope="col">Address</th>
      <th scope="col">Post Code</th>
      <th scope="col">Kota</th>
      <th scope="col">Provinsi</th>
      <th scope="col">Phone No</th>
      <th scope="col">Email</th>
    </tr>
  </thead>
  <tbody>
    <?php foreach ($history as $a): ?>
        <tr>
            <td>{{ $a->id_history }}</td>
            <td>{{ $a->first_name }}</td>
            <td>{{ $a->last_name }}</td>
            <td>{{ $a->address }}</td>
            <td>{{ $a->post_code }}</td>
            <td>{{ $a->kota }}</td>
            <td>{{ $a->provinsi }}</td>
            <td>{{ $a->phone_no }}</td>
            <td>{{ $a->email }}</td>
        </tr>
    <?php endforeach; ?>
  </tbody>
</table>
  </div>
</div>
@endsection