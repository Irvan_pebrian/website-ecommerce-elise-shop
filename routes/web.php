<?php

// Fungsi Yang Ada Di Shop Controller
Route::get('/' , 'ShopController@index');
Route::get('/shop/{key}' , 'ShopController@getView');
Route::get('/shop/keranjang/{id}' , 'ShopController@toCart');
Route::get('/clear' , 'ShopController@clearCart');
Route::get('/checkout' , 'ShopController@checkOut');

Route::post('/bayar' , 'ShopController@placeOrder');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/admin' , 'AdminController@index');
Route::get('/kelola-barang' , 'AdminController@kelolaBarang');
Route::get('/barang/tambah' , 'AdminController@tambahBarang');
Route::post('/store-barang' , 'AdminController@storeBarang');
Route::post('/store-edit' , 'AdminController@editBarang');
Route::get('/barang/edit/{id}' , 'AdminController@getBarang');
Route::post('/barang/hapus/{id}' , 'AdminController@hapusBarang');

Route::get('/history-pembelian' , 'AdminController@historyPembelian');
